package cn.ijero.naughty

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.widget.NestedScrollView

/**
 * 主要用于消费内部View传递过来的嵌套滚动和嵌套Fling事件
 *
 * @author WangChun
 */
class NaughtyNestedScrollView
@JvmOverloads
constructor(
    context: Context, attrs: AttributeSet? = null
) : NestedScrollView(context, attrs) {

    private val needHandleViews = HashSet<View>()

    override fun onNestedFling(
        target: View,
        velocityX: Float,
        velocityY: Float,
        consumed: Boolean
    ): Boolean {
        return if (needHandleViews.contains(target)) {
            super.onNestedFling(target, velocityX, velocityY, true)
        } else {
            super.onNestedFling(target, velocityX, velocityY, consumed)
        }
    }

    override fun onNestedScroll(
        target: View,
        dxConsumed: Int,
        dyConsumed: Int,
        dxUnconsumed: Int,
        dyUnconsumed: Int,
        type: Int,
        consumed: IntArray
    ) {
        if (needHandleViews.contains(target)) {
            super.onNestedScroll(
                target,
                dxConsumed,
                dyConsumed,
                0,
                0,
                type,
                consumed
            )
        } else {
            super.onNestedScroll(
                target,
                dxConsumed,
                dyConsumed,
                dxUnconsumed,
                dyUnconsumed,
                type,
                consumed
            )
        }
    }

    /**
     * 添加要处理的view
     *
     * @author WangChun
     */
    fun addNaughtyView(vararg views: View) {
        needHandleViews.addAll(views)
    }
    /**
     * 移除要处理的View
     *
     * @author WangChun
     */
    fun removeNaughtyView(vararg views: View) {
        needHandleViews.removeAll(views)
    }

}